
function	init()
{
    traductFrToMa();
}

function	calcSizeXOfCanvas(globalSizeX, spaceChar, myTextLength, widthMax)
{
    var sizeOfCan = (globalSizeX + spaceChar) * myTextLength;
    if (sizeOfCan >= widthMax)
	return(widthMax);
    else
	return(sizeOfCan);
}

function	calcSizeYOfCanvas(globalSizeX, spaceChar, myTextLength, widthMax, globalSizeY, spaceLine)
{
    var sizeXOfCan = (globalSizeX + spaceChar) * myTextLength;
    var sizeYOfCan = 0;
    var nbLine = 0;

    nbLine = parseInt(sizeXOfCan / widthMax);

    if (nbLine <= 0)
	sizeYOfCan = (globalSizeY + spaceLine);
    else
	sizeYOfCan = (nbLine +1) * (globalSizeY + spaceLine);

    return(sizeYOfCan);
}


function	traductFrToMa()
{
    /* INIT */

    var c = document.getElementById("myResult");
    var ctx = c.getContext("2d");
    var myText = document.getElementById("textFr").value;
    myText = myText.replace(/[^A-Za-z]{0-9}/g, ' '); // retire tout les caractères non alpha-numérique


    var a_func = "";
    var global_size_x = 20;
    var global_size_y = 20;

    var x_alph = global_size_x/10;
    var y_alph = global_size_x/10;;

    var space_char = 5;
    var space_line = 5;

    // Size of canvas

    var sizeXofCan = calcSizeXOfCanvas(global_size_x, space_char, myText.length, $("#textFr").width());
    ctx.canvas.width = sizeXofCan;

    ctx.canvas.height = calcSizeYOfCanvas(global_size_x, space_char, myText.length, $("#textFr").width(),  global_size_y, space_line);




    // INITIALISATION DU DICTIONNAIRE
    var da_alphabet =
	{
	    a_space: function(ctx, x_alph, y_alph)// ' '
	    {
		// Ne fait rien

	    },
	    a_error: function(ctx, x_alph, y_alph)// ' '
	    {
		ctx.fillRect(x_alph,
			     y_alph,
			     global_size_x,
			     global_size_y);

		// Ne fait rien

	    },

	    a_0: function(ctx, x_alph, y_alph) // /_\ => 0
	    {
		// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y );
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph);
		// Diagonal \
	    	ctx.moveTo(x_alph + global_size_x/2, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// ligne _
	    	ctx.moveTo(x_alph, y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
	    },
	    a_1: function(ctx, x_alph, y_alph) // \-/ => 1
	    {
		// Diagonal \
	    	ctx.moveTo(x_alph, y_alph );
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);

		// Diagonal /
	    	ctx.moveTo(x_alph + global_size_x, y_alph);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);
		// ligne -
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
	    },
	    a_2: function(ctx, x_alph, y_alph) // \.-/ => 2
	    {
		// Diagonal \
	    	ctx.moveTo(x_alph, y_alph );
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);

		// Diagonal /
	    	ctx.moveTo(x_alph + global_size_x, y_alph);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);
		// ligne _
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);

	    },
	    a_3: function(ctx, x_alph, y_alph) // \|/ => 3
	    {
		// Diagonal \
	    	ctx.moveTo(x_alph, y_alph );
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);

		// Diagonal /
	    	ctx.moveTo(x_alph + global_size_x, y_alph);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);
		// ligne |
	    	ctx.moveTo(x_alph + global_size_x/2, y_alph);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y );
	    },

	    a_4: function(ctx, x_alph, y_alph) // |> => 4
	    {
	    	// Diagonal \
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);
		// Diagonal /
	    	ctx.moveTo(x_alph , y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);

		// ligne |
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph, y_alph + global_size_y );
	    },

	    a_5: function(ctx, x_alph, y_alph) // |.> => 5
	    {
	    	// Diagonal \
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);
		// Diagonal /
	    	ctx.moveTo(x_alph , y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);

		// ligne |
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph, y_alph + global_size_y );
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
	    },
	    a_6: function(ctx, x_alph, y_alph) // -> => 6
	    {
	    	// Diagonal \
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);
		// Diagonal /
	    	ctx.moveTo(x_alph , y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);

		// Ligne -
	    	ctx.moveTo(x_alph , y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);

	    },

	    a_7: function(ctx, x_alph, y_alph) // <| => 7
	    {
	    	// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
		// Diagonal \
	    	ctx.moveTo(x_alph , y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

		// Ligne |
	    	ctx.moveTo(x_alph + global_size_x , y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

	    },

	    a_8: function(ctx, x_alph, y_alph) // <.| => 8
	    {
	    	// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
		// Diagonal \
	    	ctx.moveTo(x_alph , y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

		// Ligne |
	    	ctx.moveTo(x_alph + global_size_x , y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);

	    },

	    a_9: function(ctx, x_alph, y_alph) // <- => 9
	    {
	    	// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
		// Diagonal \
	    	ctx.moveTo(x_alph , y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

		// Ligne -
		ctx.moveTo(x_alph , y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y/2);
	    },

	    a_a: function(ctx, x_alph, y_alph)//_|
	    {
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
	    },
	    a_b: function(ctx, x_alph, y_alph)// ._|
	    {
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);


	    },
	    a_c: function(ctx, x_alph, y_alph)// |_|
	    {
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);

		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph , y_alph + global_size_y);
	    },
	    a_d: function(ctx, x_alph, y_alph)//|_.|
	    {
;
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph , y_alph + global_size_y);
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);


	    },


	    a_e: function(ctx, x_alph, y_alph)//|_
	    {
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph , y_alph + global_size_y);
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
	    },
	    a_f:function(ctx, x_alph, y_alph) // |_.
	    {

		// trait droite
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph , y_alph + global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
	    },
	    a_g:function(ctx, x_alph, y_alph) // ]
	    {

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
	    },
	    a_h:function(ctx, x_alph, y_alph) // .]
	    {

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
	    },
	    a_i:function(ctx, x_alph, y_alph) // [.]
	    {
		// rectangle
		ctx.rect(x_alph, y_alph, global_size_x, global_size_y);

	    },
	    a_k:function(ctx, x_alph, y_alph) // |_|
	    {
		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);

	    },


	    a_l:function(ctx, x_alph, y_alph) // [.]
	    {
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
		// rectangle
		ctx.rect(x_alph, y_alph, global_size_x, global_size_y);

	    },

	    a_m:function(ctx, x_alph, y_alph) // [
	    {

		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);

	    },
	    a_n:function(ctx, x_alph, y_alph) // [.
	    {

		// trait bas
		ctx.moveTo(x_alph, y_alph + global_size_y);
		ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
	    },
	    a_o:function(ctx, x_alph, y_alph) // -|
	    {
		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
	    },
	    a_p:function(ctx, x_alph, y_alph) // .-|
	    {
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
	    },
	    a_q:function(ctx, x_alph, y_alph) // |-|
	    {

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);

	    },

	    a_r:function(ctx, x_alph, y_alph) // |-.|
	    {

		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait droite
		ctx.moveTo(x_alph + global_size_x, y_alph);
		ctx.lineTo(x_alph + global_size_x , y_alph + global_size_y);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);

	    },

	    a_s: function(ctx, x_alph, y_alph) // |-
	    {
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, y_alph + global_size_y);
		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
	    },
	    a_t: function(ctx, x_alph, y_alph) //|-.
	    {
		// trait haut
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph + global_size_x, y_alph);
		// trait gauche
		ctx.moveTo(x_alph, y_alph);
		ctx.lineTo(x_alph, global_size_y);
		// Point
		ctx.fillRect(x_alph + global_size_x/2 - global_size_x/8,
			     y_alph + global_size_y/2 - global_size_y/8,
			     global_size_x/4,
			     global_size_y/4);
	    },
	    a_u: function(ctx, x_alph, y_alph) // V ==> MAINGUY
	    {
	    	// Diagonal \
	    	ctx.moveTo(x_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);
	    	// Diagonal /
	    	ctx.moveTo(x_alph + global_size_x/2, y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x, y_alph);
	    },
	    /*
	     v => u
	     w => vv
	    */


	    a_x: function(ctx, x_alph, y_alph) // < ==> MAINGUY
	    {
	    	// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y/2 );
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph);
	    	// Diagonal \
	    	ctx.moveTo(x_alph, y_alph + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph +  + global_size_y);
	    },
	    a_y: function(ctx, x_alph, y_alph) // ^ (a changer) => MAINGUY
	    {

		// Diagonal /
	    	ctx.moveTo(x_alph, y_alph + global_size_y);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph);

	    	// Diagonal \
	    	ctx.moveTo(x_alph + global_size_x/2, y_alph, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph + global_size_y);
	    },
	    a_z: function(ctx, x_alph, y_alph) // >  (A changer) => MAINGUY
	    {
	    	// Diagonal \
	    	ctx.moveTo(x_alph + global_size_x/2, y_alph);
	    	ctx.lineTo(x_alph + global_size_x, y_alph  + global_size_y/2);
		// Diagonal /
	    	ctx.moveTo(x_alph + global_size_x, y_alph  + global_size_y/2);
	    	ctx.lineTo(x_alph + global_size_x/2, y_alph + global_size_y);

	    }


	};
    // ------ FIN DU DICTIONNAIRE


    // Pour le Refresh
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvasheight); // clear tout
    ctx.beginPath();

    // FOND Blanc
    ctx.fillStyle="white";
    ctx.fillRect(0,0,ctx.canvas.width, ctx.canvas.height); // met un fond blanc
    ctx.fillStyle="black"; // remise/écriture en noir


    /* Main loop */
    for (i = 0; myText[i]; i++)
    {

	// vérification si on arrive au bout de la ligne
	if ( x_alph + global_size_x >= $("#textFr").width() )
	{
    	    x_alph = space_char;
    	    y_alph = y_alph + global_size_y + space_line;
	}

	// Mettre la lettre en minuscule
	var daLetter = myText[i].toLowerCase();


	// Ceci est moche :
	if (daLetter === ' ')
	{
	    a_func = "a_space" ;
	}
	else if (daLetter==='v')
	{
	    a_func = "a_u" ;
	    da_alphabet[a_func](ctx, x_alph, y_alph);
	}
	else if (daLetter==='w')
	{
	    a_func = "a_u" ;
	    da_alphabet[a_func](ctx, x_alph, y_alph);
	    x_alph = x_alph + global_size_x + space_char;

	}
	else if (daLetter==='j' )
	{
	    a_func = "a_i" ;
	    da_alphabet[a_func](ctx, x_alph, y_alph);
	}
	else
	{
	    a_func = "a_" + daLetter ;
	}

	da_alphabet[a_func](ctx, x_alph, y_alph); // Appeler la fonction
	x_alph = x_alph + global_size_x + space_char; // Mettre un espace après la ligne

    }

    ctx.stroke(); // Affichage
    var dataURL = c.toDataURL();

    document.getElementById('linkImg').href = dataURL;
    document.getElementById('downloadLink').href = dataURL;
    document.getElementById('downloadLink').download = myText+".png";
}
